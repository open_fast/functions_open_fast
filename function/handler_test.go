package function

import (
	"encoding/json"
	"fmt"
	"testing"
	"time"
)

func TestGetOffer(t *testing.T) {

	datanow := time.Now()

	times:= datanow.Format("2006-01-02 15:04:05")
	
	var req = Data{
		Data: RequestData{
			DepartureCode: "TOS",
			DepartureCity: nil,
			ArrivalCode:   "MOW",
			ArrivalCity:   nil,
			Date:          times,
		},
	}

	request, err := json.Marshal(req)

	if err != nil {
		fmt.Println("err: ", err)
	}

	response := Handle(request)

	fmt.Println("response: ", response)
}
