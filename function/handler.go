package function

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"time"
)

type Data struct{
	Data RequestData `json:"data"`
}

type RequestData struct {
	DepartureCode string      `json:"departure_code"`
	DepartureCity interface{} `json:"departure_city"`
	ArrivalCode   string      `json:"arrival_code"`
	ArrivalCity   interface{} `json:"arrival_city"`
	Date          string      `json:"date"`
}

type ResponseData struct {
	Status      string `json:"status"`
	Description string `json:"description"`
	Data        struct {
		TableSlug string `json:"table_slug"`
		Data      struct {
			Data struct {
				ArrivalCity   string    `json:"arrival_city"`
				ArrivalCode   string    `json:"arrival_code"`
				Date          time.Time `json:"date"`
				DepartureCity string    `json:"departure_city"`
				DepartureCode string    `json:"departure_code"`
				GUID          string    `json:"guid"`
			} `json:"data"`
		} `json:"data"`
	} `json:"data"`
}

type Response struct {
	Status string      `json:"status"`
	Data   RequestData `json:"data"`
	Error  error       `json:"error"`
}

func Handle(req []byte) string {

	var (
		request      Data
		response     Response
	)


	err := json.Unmarshal(req, &request)
	if err != nil {
		response.Error = err
		response.Status = "error"
		responseByte, _ := json.Marshal(response)
		return string(responseByte)
	}


	requestobjct := RequestData{
		DepartureCode: request.Data.DepartureCode,
		DepartureCity: request.Data.DepartureCity,
		ArrivalCode:   request.Data.ArrivalCode,
		ArrivalCity:   request.Data.ArrivalCity,
		Date:          request.Data.Date,
	}

	var requestS = Data{
		Data: requestobjct,
	}



	res, err := DoRequest("your url", "POST", "", requestS)

	if err != nil {
		response.Error = err
		response.Status = "error"
		responseByte, _ := json.Marshal(response)
		return string(responseByte)
	}

	return string(res)
}

func DoRequest(url string, method, auth string, body interface{}) ([]byte, error) {

	data, err := json.Marshal(&body)

	if err != nil {
		return nil, err
	}

	client := &http.Client{
		Timeout: time.Duration(10 * time.Second),
	}

	request, err := http.NewRequest(method, url, bytes.NewBuffer(data))
	if err != nil {
		return nil, err
	}

	request.Header.Add("X-API-Key", "your key")
	request.Header.Add("Authorization", "your api")

	resp, err := client.Do(request)

	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	respByte, err := io.ReadAll(resp.Body)

	if err != nil {
		return nil, err
	}

	return respByte, nil
}
